const Phaser = require('phaser')

// your MainScene
class MainScene extends Phaser.Scene {
  constructor() {
    super('MainScene')
  }
  create() {
    console.log('it works!')
  }
}

// prepare the config for Phaser
const config = {
  type: Phaser.AUTO,
  width: 1280,
  height: 720,
  banner: false,
  audio: false,
  scene: [MainScene],
  fps: {
    target: 30
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 }
    }
  }
}

// start the game
new Phaser.Game(config)